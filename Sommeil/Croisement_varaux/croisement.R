##############           CHARGEMENT DES DONNEES         ####################

library(readr)
load("C:/Users/MAISONNAVE/Documents/ENSAI/2A/Projet stat/text-mining/Sommeil/donnees_propres/mat_variable_quali.rda")
data <- read_delim("Donnees/Covid-19 sommeil et apnée du sommeil  comment le vivez-vous - UTF8.csv", delim=";")
#on enlève les valeurs manquantes
data <- data.frame(data[-1,])
data_sansNA <- data[!(is.na(data[,58])),]

#on merge dans une meme table els variables auxiliaires et la variable quali crée précedemment. 
table <- cbind(data_sansNA, mat_variable_quali$vect.fact)

############          PREPARATION DES VARIABLES       ################

#Souffrez.vous.d.apnée.du.sommeil.. : tous les individus souffrent d'apnée

intensite_apnee <- as.factor(table$Votre.apnée.du.sommeil.est.plutôt..)
sexe <- as.factor(table$Vous.êtes..)
age <- as.factor(table$Quel.âge.avez.vous..)
covid_bin <- as.factor(table$Avez.vous.été.infecté.e..par.le.COVID.19..)
ppc <- as.factor(table$Quel.traitement.utilisez.vous..)
# il manque beaucoup trop de valeurs pour le poids
#il manque beaucoup trop de valeurs pour la taille
sieste <- as.factor(table$En.confinement..avez.vous.modifié.vos.habitudes.de.siestes..)
dodo_partenaire <- as.factor(table$Avec.votre.partenaire..avez.vous.modifié.vos.habitudes.pendant.le.confinement....si.vous.n.avez.pas.de.partenaire..passez.la.question.)
sommeil_confi <- mat_variable_quali$vect.fact

table <- data.frame(cbind(intensite_apnee,sexe,age,covid_bin,ppc,sieste,dodo_partenaire,sommeil_confi))

#################################################################################################
#################                 STATISTIQUES UNIVARIEES                    ####################
#################################################################################################

table(table$sexe) #homme:  7629  femme:   3689 
table(table$covid_bin) # N'a pas eu le covid : 11051 A eu le covid 267 
table(table$age) 
#Entre 31 et40 ans : 414 
#Entre 41 et 50 ans: 1273 
#Entre 51 et 60 ans: 2581 
#Moins de 30 ans ou 30 ans:85 
#Plus de 60 ans :6965      

table(table$intensite_apnee)
#"Je ne sais pas" : 1392                                   
#"Légère (nombre d'apnées hypopnées entre 5 et 15)" : 2245 
#"Modérée (nombre d'apnées hypopnées entre 15 et 30)" : 2330
#"Sévère (nombre d'apnée hypopnées supérieur à 30)" : 5341

table(table$ppc)
#"Une machine à pression positive continue (PPC)": 265
#"Une orthèse d'avancée mandibulaire (OAM)" :3

table(table$dodo_partenaire)
#"Autre (veuillez préciser)"  : 1166                                              
#"Non, nous dormons toujours dans deux chambres séparées"   : 1430                    
#"Non, nous dormons toujours dans la même chambre"      : 6967                      
#"Oui, nous dormons dans des chambres séparées depuis le début de l'épidémie" : 430  

table(table$sieste)
#"Non, j'ai toujours fait des siestes et continue à en faire depuis le début du confinement"  3434  
#"Non, je n'ai jamais fait la sieste et n'en fais pas non plus depuis le début du confinement" 5354  
#"Oui, je fais moins de siestes"  857                                                            
#"Oui, je fais plus de siestes" 1673 


####################################################################################
#########################    STATISTIQUES BIVARIEES     ############################
####################################################################################

##############################      SEXE         ###################################
#effectifs croisés
tab1 <- table(sommeil = table$sommeil_confi,sexe = table$sexe)

#ajoute des totaux en ligne et en colonne
tab2 <- addmargins(tab1,FUN=list(Total=function(x) sum(x)))

PourcentageLigne <- matrix(0,nrow(tab1),ncol(tab1)+1)
for (i in 1:nrow(tab1)){
    PourcentageLigne[i,1]<- round((tab1[i,1]/sum(tab1[i,]))*100,1)
    PourcentageLigne[i,2]<- round((tab1[i,2]/sum(tab1[i,]))*100,1)
    PourcentageLigne[i,3]<- round((sum(tab1[i,])/sum(tab1))*100,1)
}

colnames(PourcentageLigne)<- c("1","2","Total")
rownames(PourcentageLigne)<- c("1","2","3","4")

PourcentageColonne <- matrix(0,nrow(tab1)+1,ncol(tab1))
for (i in 1:ncol(tab1)){
  PourcentageColonne[1,i]<- round((tab1[1,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[2,i]<- round((tab1[2,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[3,i]<- round((tab1[3,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[4,i]<- round((tab1[4,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[5,i]<- round(sum(tab1[,i]/sum(tab1))*100,1)
}

colnames(PourcentageColonne)<-c("1","2")
rownames(PourcentageColonne)<- c("1","2","3","4","Total")

#On assemble les pourcentages totaux pour comparer facilement
PourcentageLigne_fin <- rbind(PourcentageLigne[,-3], Total =PourcentageColonne[5,])
PourcentageColonne_fin <- cbind(PourcentageColonne[-5,], Total =PourcentageLigne[,3])


##############################      AGE         ###################################
#effectifs croisés
tab1 <- table(sommeil = table$sommeil_confi,age = table$age) #modas de 4 dans age trop faible : on 
#regroupe avec la 1 car ce sont les moins de quarante ans. 
tab1[,1] <- tab1[,1]+tab1[,4]
tab1 <- tab1[,-4]
colnames(tab1)<- c("1 et 4", "2", "3","5")

#ajoute des totaux en ligne et en colonne
tab2 <- addmargins(tab1,FUN=list(Total=function(x) sum(x)))

PourcentageLigne <- matrix(0,nrow(tab1),ncol(tab1)+1)
for (i in 1:nrow(tab1)){
  PourcentageLigne[i,1]<- round((tab1[i,1]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,2]<- round((tab1[i,2]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,3]<- round((tab1[i,3]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,4]<- round((tab1[i,4]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,5]<- round((sum(tab1[i,])/sum(tab1))*100,1)
  
}

colnames(PourcentageLigne)<- c("1 et 4","2","3","5","Total")
rownames(PourcentageLigne)<- c("1","2","3","4")

PourcentageColonne <- matrix(0,nrow(tab1)+1,ncol(tab1))
for (i in 1:ncol(tab1)){
  PourcentageColonne[1,i]<- round((tab1[1,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[2,i]<- round((tab1[2,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[3,i]<- round((tab1[3,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[4,i]<- round((tab1[4,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[5,i]<- round(sum(tab1[,i]/sum(tab1))*100,1)
}

colnames(PourcentageColonne)<-c("1 et 4","2","3","5")
rownames(PourcentageColonne)<- c("1","2","3","4","Total")

#On assemble les pourcentages totaux pour comparer facilement
PourcentageLigne_fin <- rbind(PourcentageLigne[,-5], Total =PourcentageColonne[5,])
PourcentageColonne_fin <- cbind(PourcentageColonne[-5,], Total =PourcentageLigne[,5])

##############################      INTENSITE APNEE         ###################################
#effectifs croisés
tab1 <- table(sommeil = table$sommeil_confi,intensite = table$intensite_apnee) 

#ajoute des totaux en ligne et en colonne
tab2 <- addmargins(tab1,FUN=list(Total=function(x) sum(x)))

PourcentageLigne <- matrix(0,nrow(tab1),ncol(tab1)+1)
for (i in 1:nrow(tab1)){
  PourcentageLigne[i,1]<- round((tab1[i,1]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,2]<- round((tab1[i,2]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,3]<- round((tab1[i,3]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,4]<- round((tab1[i,4]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,5]<- round((sum(tab1[i,])/sum(tab1))*100,1)
  
}

colnames(PourcentageLigne)<- c("1","2","3","4","Total")
rownames(PourcentageLigne)<- c("1","2","3","4")

PourcentageColonne <- matrix(0,nrow(tab1)+1,ncol(tab1))
for (i in 1:ncol(tab1)){
  PourcentageColonne[1,i]<- round((tab1[1,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[2,i]<- round((tab1[2,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[3,i]<- round((tab1[3,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[4,i]<- round((tab1[4,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[5,i]<- round(sum(tab1[,i]/sum(tab1))*100,1)
}

colnames(PourcentageColonne)<-c("1","2","3","4")
rownames(PourcentageColonne)<- c("1","2","3","4","Total")

#On assemble les pourcentages totaux pour comparer facilement
PourcentageLigne_fin <- rbind(PourcentageLigne[,-5], Total =PourcentageColonne[5,])
PourcentageColonne_fin <- cbind(PourcentageColonne[-5,], Total =PourcentageLigne[,5])

##############################      SIESTE         ###################################
#effectifs croisés
tab1 <- table(sommeil = table$sommeil_confi,sieste = table$sieste) 

#ajoute des totaux en ligne et en colonne
tab2 <- addmargins(tab1,FUN=list(Total=function(x) sum(x)))

PourcentageLigne <- matrix(0,nrow(tab1),ncol(tab1)+1)
for (i in 1:nrow(tab1)){
  PourcentageLigne[i,1]<- round((tab1[i,1]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,2]<- round((tab1[i,2]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,3]<- round((tab1[i,3]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,4]<- round((tab1[i,4]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,5]<- round((sum(tab1[i,])/sum(tab1))*100,1)
  
}

colnames(PourcentageLigne)<- c("1","2","3","4","Total")
rownames(PourcentageLigne)<- c("1","2","3","4")

PourcentageColonne <- matrix(0,nrow(tab1)+1,ncol(tab1))
for (i in 1:ncol(tab1)){
  PourcentageColonne[1,i]<- round((tab1[1,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[2,i]<- round((tab1[2,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[3,i]<- round((tab1[3,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[4,i]<- round((tab1[4,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[5,i]<- round(sum(tab1[,i]/sum(tab1))*100,1)
}

colnames(PourcentageColonne)<-c("1","2","3","4")
rownames(PourcentageColonne)<- c("1","2","3","4","Total")

#On assemble les pourcentages totaux pour comparer facilement
PourcentageLigne_fin <- rbind(PourcentageLigne[,-5], Total =PourcentageColonne[5,])
PourcentageColonne_fin <- cbind(PourcentageColonne[-5,], Total =PourcentageLigne[,5])


##############################      DODO PARTENAIRE        ###################################
#effectifs croisés
tab1 <- table(sommeil = table$sommeil_confi,dodoPartenaire = table$dodo_partenaire) 

#ajoute des totaux en ligne et en colonne
tab2 <- addmargins(tab1,FUN=list(Total=function(x) sum(x)))
#Attention, effectifs faibles pour moda 4 ! "Oui, nous dormons dans des chambres séparées depuis le début de l'épidémie"

PourcentageLigne <- matrix(0,nrow(tab1),ncol(tab1)+1)
for (i in 1:nrow(tab1)){
  PourcentageLigne[i,1]<- round((tab1[i,1]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,2]<- round((tab1[i,2]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,3]<- round((tab1[i,3]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,4]<- round((tab1[i,4]/sum(tab1[i,]))*100,1)
  PourcentageLigne[i,5]<- round((sum(tab1[i,])/sum(tab1))*100,1)
  
}

colnames(PourcentageLigne)<- c("1","2","3","4","Total")
rownames(PourcentageLigne)<- c("1","2","3","4")

PourcentageColonne <- matrix(0,nrow(tab1)+1,ncol(tab1))
for (i in 1:ncol(tab1)){
  PourcentageColonne[1,i]<- round((tab1[1,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[2,i]<- round((tab1[2,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[3,i]<- round((tab1[3,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[4,i]<- round((tab1[4,i]/sum(tab1[,i]))*100,1)
  PourcentageColonne[5,i]<- round(sum(tab1[,i]/sum(tab1))*100,1)
}

colnames(PourcentageColonne)<-c("1","2","3","4")
rownames(PourcentageColonne)<- c("1","2","3","4","Total")

#On assemble les pourcentages totaux pour comparer facilement
PourcentageLigne_fin <- rbind(PourcentageLigne[,-5], Total =PourcentageColonne[5,])
PourcentageColonne_fin <- cbind(PourcentageColonne[-5,], Total =PourcentageLigne[,5])

