load(file="Sommeil/donnees_propres/mat_variable_quali.rda")

#on garde uniquement la variable qualitative
data <- data.frame(mat_variable_quali[,2405])

colnames(data)<- "qualite_sommeil"

library(ggplot2)
# barplot
graph <- ggplot(data, aes(x=qualite_sommeil)) + 
  geom_bar(aes(y = (..count..)/sum(..count..)),fill = "gray15", width = .7) 
# barplot et titres
graph <- graph + ggtitle("De manière générale, le confinement a-t-il eu des répercutions sur votre sommeil?") + xlab("Qualité du sommeil") + ylab("") 
# barplot et titres en joli ! 
graph <-graph + theme(plot.title = element_text(face="bold", color="gray20", size=10.5, hjust=0.5)) + 
  theme(axis.title.x = element_text(face = "bold", colour= "gray15", size= 9))

#modifier l'unité des ordonnées 
library(scales)
# Pourcentage
graph <- graph + scale_y_continuous(labels = percent) 

#modification des lignes 
graph <- graph + theme(
  panel.grid.major = element_line(size = 0.5, linetype = 'dashed',
                                  colour = "gray20"), 
  panel.grid.minor = element_line(size = 0.25, linetype = 'solid',
                                  colour = "white"),
  panel.grid.major.x = element_line(colour="white", linetype="solid"), # Lignes verticales principales du quadrillage
)

# changer le nom des modalités 
graph + scale_x_discrete(labels=c("0" = "Inclassable", "1" = " Aucun changement \n ou amélioration",
                              "2" = "Détérioration ", "3" = "Pas d'information \n sur la nature du changement" ))
  








