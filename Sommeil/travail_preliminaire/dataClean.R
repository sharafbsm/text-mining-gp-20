#Chargement des packages
library("tm")
library("tidytext")
library("tidyverse")
library("stopwords")

#Importer les données
library(readxl)
dodo_q <- data.frame(read_excel("Sommeil/Q39_Text.xlsx",range = "A3:C11942"))
dodo_q <- dodo_q[-1,] #ligne inutile

#Conversion en corpus
dodo <- data.frame(doc_id = dodo_q$Respondents,text=dodo_q$Responses)
source <- DataframeSource(dodo)
dodo <- VCorpus(source)

### CLEAN LA BASE DODO
#espaces en trop
dodo <- tm_map(dodo, stripWhitespace)
#en minuscule
dodo <- tm_map(dodo, content_transformer(tolower))
#stopwords
dodo <- tm_map(dodo,removeWords, setdiff(c(stopwords("french"),"donc","alors",
                                           "actuel","actuellement","actuelle", 
                                           "ailleurs","ainsi",
                                           "apparemment","appoaremment",
                                           "apres","apresmidi","apresmidis",
                                           "apriori", "assez","aucunement","aucunementhabitant",
                                           "aujourdhui","auparavant","auparavent", "aupres",
                                           "aussitôt","aussi", "celles","celui","cependant",
                                           "certain","certaines","certaines",'certainement',"certains",
                                           "ceux", "deja","dela","demain","desormais",
                                            ,"vers","quelques","car", "comme"),c('pas', 'sans')))
#enlever les nombre
dodo <- tm_map(dodo, removeNumbers)
#enlever la ponctuation
dodo <- tm_map(dodo, removePunctuation)
#enlever les accents
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "é|è|ê|ë", replacement = "e") 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "à|â|ä", replacement = "a") 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "ù,û|ü", replacement = "u") 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "’", replacement = "") 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "´", replacement = "") 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "ñ", replacement = "n")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\U0001f60d", replacement = "")

#regroupement manuel de mots 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(acceptera)\\b", replacement = "accepter") 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(accroit|accrues|accrus)\\b", replacement = "accroitre") 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(active)\\b", replacement = "actif") 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(activiter|activites)\\b", replacement = "activite")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(adaptation|adaptes)\\b", replacement = "adapter") 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(affectation|affecte|affectee)\\b", replacement = "affecter") 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(aggravait|aggravation|aggrave|aggraves)\\b", replacement = "aggraver") 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(agitant|agite|agiteanxiete|agitee|agitees|agites)\\b", replacement = "agitation")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(aider)\\b", replacement = "aide")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(aigues|soutenu|soutenue)\\b", replacement = "aigue")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(allais|allait|allant|allee)\\b", replacement = "aller")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(allergies|allergiques)\\b", replacement = "allergie")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(allonge|allongee|allongement|allonges)\\b", replacement = "allonger")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(alteree|alteres)\\b", replacement = "altere")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(ambiance|ambiante)\\b", replacement = "ambiant")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(ameliorations|ameliore|amelioree|ameliorees|ameliorer)\\b", replacement = "amelioration")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(ampleur|amplitudes)\\b", replacement = "amplitude")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(amis)\\b", replacement = "ami")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(analyses)\\b", replacement = "analyse")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(engoise|parfoisangoisse|engoisse|dangoisse|angoises|angoissant|angoissants|angoissee|angoissent|angoisser|angoisses|anxiete|anxieuse|anxieux|anxiogene|anxiosite|anxyogene)\\b", 
               replacement = "angoisse")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(annees)\\b", replacement = "annee")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(annonces)\\b", replacement = "annonces")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(annules)\\b", replacement = "annule")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(apneees|apnees)\\b", replacement = "apnee")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(appareillage|apppareil|appareille|appareillee|appareilles)\\b", replacement = "appareil")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(apparente|apparition|apparent)\\b", replacement = "apparaitre")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(appart|appartements)\\b", replacement = "appartement")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(appel)\\b", replacement = "appeler")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(apportees)\\b", replacement = "apporte")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(areter|arret|arrete|arretees)\\b", replacement = "arreter")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(arrivais|arrivant|arrive|arrivee|arrives)\\b", replacement = "arriver")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(attends|attente)\\b", replacement = "attendre")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(attrapper)\\b", replacement = "attraper")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(auc|aucine|aucune|aucunes|aucunne|aucunr|aucuns)\\b", replacement = "aucun")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(augmentation|augmente|augmentee|augmentees|augmentent)\\b", replacement = "augmenter")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(avantages)\\b", replacement = "avantage")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(avoirs)\\b", replacement = "avoir")

#reprendre à la lettre b 
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(baisse)\\b", replacement = "baisser")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(barrieres)\\b", replacement = "barriere")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(bbeaucoup|bcp|bcps|beacoup|beaucoups)\\b", replacement = "beaucoup")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(besoins)\\b", replacement = "besoin")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(blanches|blanche)\\b", replacement = "blanc")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(bons|bonne)\\b", replacement = "bon")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(besoins)\\b", replacement = "besoin")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(bricolage|bricole)\\b", replacement = "bricoler")

dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(cachets)\\b", replacement = "cachet")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(calmes)\\b", replacement = "calme")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(cardiaque|cardiologue)\\b", replacement = "cardiologie")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(cauchemards|cauchemares|cauchemars|cauchermards)\\b", replacement = "cauchemar")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(change|changeant|changee|changees|changements|changent|changer|changera)\\b", replacement = "changement")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(commence|commencement|commencent)\\b", replacement = "commencer")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(complexe|complexite|complications|compliquee|compliquees|compliques)\\b", replacement = "complique")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(confine|confinee|confinees|confinelment|confinelol|confinemebt|confinemen|conflnement|confinements|confines)\\b", replacement = "confinement")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(confirme|confirmation)\\b", replacement = "confirmer")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(connais|connaissais|connaître)\\b", replacement = "connaissance")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(conseille|conseiller|conseils)\\b", replacement = "conseil")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(onsequences|consquences|consequences)\\b", replacement = "consequence")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(confirme|confirmation)\\b", replacement = "confirmer")

dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(contacte)\\b", replacement = "contacter")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(contaminee)\\b", replacement = "contamination")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(continuant|continuation|continue|continuer)\\b", replacement = "continu")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(contraignant|contraintes)\\b", replacement = "contrainte")

#ça devient long, je sélectionne moins de mots : en lien avec la question
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(couchers|couchais|couchant|couche|couche|couchees)\\b", replacement = "coucher")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(courte|courtes)\\b", replacement = "court")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(apprehension|crains|craintes)\\b", replacement = "crainte")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(crise)\\b", replacement = "crise")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(decalage|decalages|decale|decalee|decalees|decales|decaleragite|decallageperte|decalle)\\b", replacement = "decaler")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(degrade|degradee)\\b", replacement = "degradation")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(crise)\\b", replacement = "crise")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(deprimee|deprimer)\\b", replacement = "deprime")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(deregler|dereglees|dereglement|dereglements)\\b", replacement = "deregle")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(detendu|detendue)\\b", replacement = "detente")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(differemment|different|differente|differentes|differents)\\b", replacement = "difference")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(ouidificulte|ouidifficultes|difficile|difficilement|difficiles|difficilestress|difficultees|difficultes)\\b", replacement = "difficulte")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(ecourtee|diminue|diminuee|diminuer)\\b", replacement = "diminution")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(dore|dormais|dorme|dormeuse|dormi|dormions|dormirsinon|dormons|dors|dort|dorts)\\b", replacement = "dormir")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(douleurs|douloureuses)\\b", replacement = "douleur")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(mmendormir|mendormir|endormais|endormie|endormirparfois|endormirrmir|endormis|endormissement|endormissements|endormissent|endors|endort|endromissement)\\b", replacement = "endormir")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(enerve|enervee)\\b", replacement = "enervement")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(epuise|epuisee)\\b", replacement = "epuisement")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(essai|essaie|essais|essayais|essaye)\\b", replacement = "essayer")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(etouffe|etouffements|etouffer)\\b", replacement = "etouffement")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(facile|faciles)\\b", replacement = "facilement")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(facile|faciles)\\b", replacement = "facilement")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(fair|fais|faisais|faisait|faisant|fait|faite|faites|faits)\\b", replacement = "faire")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(fatigantes|fatiguee|fatiguer|fatigues|fatiguescar|fatiguesuite)\\b", replacement = "fatigue")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(frequemment|frequente|frequentes|frequents)\\b", replacement = "frequent")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(genait|genant|genante|genee|genent)\\b", replacement = "gene")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(gerables|gere|gerees)\\b", replacement = "gerer")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(facile|faciles)\\b", replacement = "facilement")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(grave|graves|severe)\\b", replacement = "gravite")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(grosses|grosse)\\b", replacement = "gros")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(habitudea|habitudes|habitue|habituee|habituel|habituelle|habituellement|habituelles|habituels|habituer)\\b", replacement = "habitude")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(hacheanxiete|saccade|sacade|hachees|haches|hachure|hachurer|intermitent|intermittent|interruptions|intermittant)\\b", replacement = "interruption")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(horaire|horaires|horraire|heures)\\b", replacement = "heure")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(importance|importante|importantes|importants)\\b", replacement = "important")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(inhabituelle|inhabituels)\\b", replacement = "inhabituel")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(inquiet|inquiete|inquiets|inquietudes)\\b", replacement = "inquietude")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(isomnies|indomnies|insomnies|insomniaque|insomniaquepeut|insomnieanxiete|insomnies)\\b", replacement = "insomnie")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(intense|intenses)\\b", replacement = "intensite")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(irreguliere|irregulieres|irreguliers)\\b", replacement = "irregulier")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(lege|legere|legerement|legeres|legerment|legersouvent)\\b", replacement = "leger")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(logtemps|lon|longs|longtemps|longtempssinon|longue|longement|longues|lontem)\\b", replacement = "long")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(lourde|lourdeur|lourds)\\b", replacement = "loud")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(matinal|matinaux|matinee|matinees|matins)\\b", replacement = "matin")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(mauvaise|mauvaises)\\b", replacement = "mauvais")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(grosses|grosse)\\b", replacement = "gros")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(meilleure|meilleures)\\b", replacement = "meilleur")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(modifications|modifie|modifiees|modifier|modifies)\\b", replacement = "modification")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(mouvementee)\\b", replacement = "mouvemente")
#vérif les occurence de néant, souvent ça veut dire non
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(necessaires|necessitaire|necessite|necessitent)\\b", replacement = "necessaire")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(negatives|negatifs|negative)\\b", replacement = "negatif")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(nerveuse|nervosite|nerveusement)\\b", replacement = "nerveux")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(npn|nnon|nin|nob|ras)\\b", replacement = "non")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(nocture|noctures|nocturnes|nocturme)\\b", replacement = "nocturne")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(nonaucun|nonaucune|nonaucunes|nonni|nonpas|noon)\\b", replacement = "non")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(nuitdifficultes|nuitjusque|nuits)\\b", replacement = "nuit")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(oppresse|oppresse|opressaient)\\b", replacement = "oppression")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(ouï|oui…|ouicar|ouii|ouiii)\\b", replacement = "oui")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(partiellement|partie|partielle|partielles)\\b", replacement = "partiel")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(pertubations|pertube|perturbees|perturbant|perturbants|perturbation|perturbations|perturbe|perturbeagite|perturbedebuts|perturbee|perturbees|perturbent|perturbes)\\b", replacement = "perturbe")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(plsu|plu|plud)\\b", replacement = "plus")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(psychologiques|psychologiquement)\\b", replacement = "psychologique")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(quotidienne|quotidiennement|quotidiennes)\\b", replacement = "quotidien")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(rassurants|rassuree|rassurer)\\b", replacement = "rassure")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(reendormissement|reendormir|rendormir…|rendormirdur|rendormissement|rendormissements|rendors|rendort|reordormissement)\\b", replacement = "rendormir")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(regularise|regularite|regule|regulee|reguliation|regulieres|reguliers)\\b", replacement = "regulier")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(reparatrice|reparatrices)\\b", replacement = "reparateur")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(repercussion|repercute)\\b", replacement = "repercution")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(repos|repose|reposee)\\b", replacement = "reposant")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(respiratoire|respiration|respiratoires|respire)\\b", replacement = "respirer")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(ressens|ressent|ressentie|ressenties|ressentir|ressentis)\\b", replacement = "ressenti")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(reveil|reveillais|reveillant|reveille|reveillee|reveilles|reveils|reveilsplus|reveis)\\b", replacement = "reveiller")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(sereine|sereinement)\\b", replacement = "serein")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(soiree|soirees|soirs)\\b", replacement = "soir")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(someil|somleil|sommeill|sommeille|sommeils|sommeilvie)\\b", replacement = "sommeil")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(somniferes|somnipheres)\\b", replacement = "somnifere")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(stressant|sressee|stressante|stresse|stressee|stressecomme|stressperturbe)\\b", replacement = "stress")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(supportais|supportait|supportant|supporte)\\b", replacement = "supporter")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(supportables)\\b", replacement = "supportable")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(tarde|tardif|tardifs|tardive|tardivement|tardives|tarrd)\\b", replacement = "tard")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(teletravaille|teletravaillant|teletravaille)\\b", replacement = "teletravail")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(tele|television|televisionsmartphone|televisions)\\b", replacement = "ecran")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(tôt)\\b", replacement = "tot")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(traitant|traite|traitee|traitements|traitementsfaut|traiter)\\b", replacement = "traitement")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(trpavail|travail|travaillais|travaillant|travaille|travaillent|travailleur)\\b", replacement = "travailler")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(troubles|troublante)\\b", replacement = "trouble")
dodo <- tm_map(dodo, content_transformer(gsub), pattern = "\\b(variables|varient|varier)\\b", replacement = "variable")


#si le mot est le participe passé d'un verbe : il vaut mieux le remplacer par un adverbe ou un nom : c'est un état, pas une action
#si le mot est globalement utilisé comme un verbe conjugué, il vaut mieux mettre le verbe à l'infinitif

save(dodo, file="C:/Users/MAISONNAVE/Documents/ENSAI/2A/Projet stat/text-mining/Sommeil/donnees_propres/dodo.rda")

## TERM DOCUMENT MATRIX
dtm <- DocumentTermMatrix(dodo)
inspect(dtm)
dtm$dimnames$Terms

save(dtm, file="C:/Users/MAISONNAVE/Documents/ENSAI/2A/Projet stat/text-mining/Sommeil/donnees_propres/dtm.rda")




