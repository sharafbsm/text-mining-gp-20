###### PROBLEME  RWEKA ###### 
Pour le pb concernant RWeka, telecharger Java ici:
https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
Puis lancer les commande :
install.packages("rJava", type = "source")
install.packages("RWeka")
library("RWeka")

###################
#dtm2 <- data.frame(as.matrix(dtm),stringsAsFactors=FALSE)

#charger la table propre 
load(file="C:/Users/MAISONNAVE/Documents/ENSAI/2A/Projet stat/text-mining/Sommeil/donnees_propres/dtm2.rda")
load(file="C:/Users/MAISONNAVE/Documents/ENSAI/2A/Projet stat/text-mining/Sommeil/donnees_propres/dtm.rda")

#création du data.frame avec les mots

colnames(dtm2) <- dtm$dimnames$Terms 
frequence_mot <- colSums(dtm2)
frequence_mot<- data.frame(frequence_mot)

#on garde les mots dont la fréquence dépasse un certain nombre
indice_mots_frequents <- which(frequence_mot>150) #stocke les indices des mots qui sont utilisés plus de 33x

#on les réassemble dans le dataframe grâce aux indices récupérés précedemment
freq <- data.frame(frequence_mot[indice_mots_frequents,])
rownames(freq) <- rownames(frequence_mot)[indice_mots_frequents]
colnames(freq) <- "frequency"

#on crée un data frame ordonné pour pouvoir faire le plot 
freq.sommeil2 <- data.frame(word = rownames(freq), frequency = freq)
freq.sommeil2$word <- reorder(freq.sommeil2$word, freq.sommeil2$frequency)
g.freq.sommeil <- ggplot(freq.sommeil2, aes(x = word, y = frequency ))
g.freq.sommeil <- g.freq.sommeil + geom_bar(stat = "identity") + coord_flip() + 
  labs(title = "Most Frequent: Sommeil ")
print(g.freq.sommeil)
