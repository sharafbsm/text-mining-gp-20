##############           CHARGEMENT DES DONNEES         ####################

library(readr)
load("C:/Users/MAISONNAVE/Documents/ENSAI/2A/Projet stat/text-mining/Sommeil/donnees_propres/mat_variable_quali.rda")
data <- read_delim("Donnees/Covid-19 sommeil et apnée du sommeil  comment le vivez-vous - UTF8.csv", delim=";")
#on enlève les valeurs manquantes
data <- data.frame(data[-1,])
data_sansNA <- data[!(is.na(data[,58])),]

#on merge dans une meme table els variables auxiliaires et la variable quali crée précedemment. 
table <- cbind(data_sansNA, mat_variable_quali$vect.fact)

############          PREPARATION DES REGRESSEURS       ################

#Souffrez.vous.d.apnée.du.sommeil.. : tous les individus souffrent d'apnée

intensite_apnee <- as.factor(table$Votre.apnée.du.sommeil.est.plutôt..)
sexe <- as.factor(table$Vous.êtes..)
age <- as.factor(table$Quel.âge.avez.vous..)
covid_bin <- as.factor(table$Avez.vous.été.infecté.e..par.le.COVID.19..)
ppc <- as.factor(table$Quel.traitement.utilisez.vous..)
# il manque beaucoup trop de valeurs pour le poids
#il manque beaucoup trop de valeurs pour la taille
sieste <- as.factor(table$En.confinement..avez.vous.modifié.vos.habitudes.de.siestes..)
dodo_partenaire <- as.factor(table$Avec.votre.partenaire..avez.vous.modifié.vos.habitudes.pendant.le.confinement....si.vous.n.avez.pas.de.partenaire..passez.la.question.)
sommeil_confi <- mat_variable_quali$vect.fact

#gestion des NA dans ppc

table_vglm <- data.frame(cbind(intensite_apnee,sexe,age,covid_bin,ppc,sieste,dodo_partenaire,sommeil_confi))
save(table_vglm, file="Sommeil/donnees_propres/table_vglm.rda")

############  PREPARATION DE LA TABLE POUR LE MODELE MULTINOMIAL #######################

library("haven")
library("VGAM")
library('reshape2')

table_vglm_reshape<-aggregate(table_vglm$sommeil_confi,by=
                                  list( table_vglm$sexe,
                                       table_vglm$age,
                                       table_vglm$intensite_apnee,
                                       table_vglm$covid_bin,
                                       table_vglm$sieste,
                                       table_vglm$dodo_partenaire)
                                ,FUN=length)
colnames(table_vglm_reshape)<-c('sexe','age','intensite_apnee','covid','sieste','dodo_partenaire','freq')
sum(table_vglm_reshape$freq) #9993 individus pris en compte : ça a enlevé les NA ! 

# décompose chaque profil avec les effectifs selon la modalité e la variable à prédire
table_vglm_reshape<-dcast(sexe + age + intensite_apnee + covid + sieste + dodo_partenaire ~ sommeil_confi,data=table_vglm_reshape)

colnames(table_vglm_reshape)[7:10]<-c('freqY0','freqY1','freqY2','freqY3')
table_vglm_reshape[is.na(table_vglm_reshape)]<-0


############   TEST DE LA SIGNIFICATIVITE DES VARIABLES UNE A UNE ###################
model_sexe <- vglm(cbind(table_vglm_reshape$freqY1,
                    table_vglm_reshape$freqY2,
                    table_vglm_reshape$freqY3,
                    table_vglm_reshape$freqY0)~ sexe,
              family=multinomial,data=table_vglm_reshape)
summary(model_sexe)
lrtest_vglm(model_sexe)

#ne fonctionne pas pour l instant
model <- vglm(cbind(table_vglm_reshape$freqY1,
                    table_vglm_reshape$freqY2,
                    table_vglm_reshape$freqY3,
                    table_vglm_reshape$freqY0)~ sexe + age + intensite_apnee + covid + sieste + dodo_partenaire,
                    family=multinomial,data=table_vglm_reshape)



model <- vglm(cbind(myFreq$freq1, myFreq$freq2, myFreq$freq3) ∼ Z + X, family = multinomial,
              data = myFreq)
summary(model)


