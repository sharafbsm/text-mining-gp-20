#On essaye de prédire les modalités égales à 0

load(file="Sommeil/donnees_propres/mat_variable_quali.rda")

#On enlève des régresseurs

i <- which(colSums(mat_variable_quali[,-2804]) > 1)
X <- mat_variable_quali[,i]
mat_variable_quali <- cbind(X, mat_variable_quali[,2804]) # plus que 1007 régresseurs

#On construit l'échantillon test et l'échantillon d'apprentissage :
# TEST : ne contient que les modalités 0
# APP : contient toutes les modalités sauf 0 

indice_app <- which(mat_variable_quali[,1159]!=0)
indice_test <- which(mat_variable_quali[,1159]==0)

app <- mat_variable_quali[indice_app,]
test <- mat_variable_quali[indice_test,]


##########      Methode des KNN : trop de régresseurs, impossible     ##############

##########        Naive Bayes : hypothèse d'indépendance              ##############
#         entre les régresseurs totalement erronnée dans notre cas 

##########                SEGMENTATION PAR ARBRE                      ########

##########################################################################
# Construire l'arbre puis élagage avec validation croisée leave one out #
##########################################################################

#chargement de la librairie Rpart
library(rpart)
#pour de plus beaux graphiques 
library(rpart.plot)
#parametres=rpart.control(minsplit=60,minbucket=30, 
                        # xval= (nrow(app)-1), # validation croisée leave one out #
                        # maxcompete = 4, maxsurrogate = 4, 
                        # usesurrogate = 2,
                        # maxdepth = 30)
# cp=0 pour avoir l’arbre maximal si on ne veut pas d'élagage

#Attention, c'est un peu long mdr 
modele=rpart( app$`mat_variable_quali[, 2804]`~.,data=app)
#, method="class",control=parametres,parms=list( split='gini'))
print(modele)#imprime les règles
rpart.plot(modele)

summary(modele)

predict(modele,test) # donne pour chaque observation les proba d'avoir 
# classe="e" en 1ere colonne ou classe="p" en 2ième colonne
# ie modalités de classe dans l'ordre alphabétique)
predtest=predict(modele,test,type="class") # donne pour chaqueobservation la modalité prédite

summary(predtest)

#on crée un vecteur avec les modalités prédites
var_new_moda <- test
var_new_moda[,1159] <- predtest

data <- rbind(app, var_new_moda)
#comment les trier par indice ??? 

save(app, file = "Sommeil/donnees_propres/app.rda")
save(var_new_moda, file = "Sommeil/donnees_propres/var_new_moda.rda")
save(data, file = "Sommeil/donnees_propres/data_a_trier.rda")

############On essaye la même chose avec moins d'observations dans l'échantillon d'app ########

##### On tire au sort un nouvel échantillon
set.seed(812)
indice_app2 <- sample(1:10760, 5969) #4x la taille de l'ech test
app2 <- app[indice_app2,]

##### same code qu'avant

#Attention, c'est un peu long mdr 
modele2=rpart( app2$`mat_variable_quali[, 2804]`~.,data=app2)
#, method="class",control=parametres,parms=list( split='gini'))
print(modele2)#imprime les règles
rpart.plot(modele2)

summary(modele2)

predict(modele2,test) # donne pour chaque observation les proba d'avoir 
# classe="e" en 1ere colonne ou classe="p" en 2ième colonne
# ie modalités de classe dans l'ordre alphabétique)
predtest2=predict(modele2,test,type="class") # donne pour chaqueobservation la modalité prédite

summary(predtest2)

#on crée un vecteur avec les modalités prédites
var_new_moda2 <- test
var_new_moda2[,1159] <- predtest2

#on rassemble toutes les données. 
data <- rbind(app, var_new_moda2)




