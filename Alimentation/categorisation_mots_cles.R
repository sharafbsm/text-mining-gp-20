library(tm)

#charger latable propre 
load(file="Alimentation/Donnees/DTM/DTM.Rdata")
load(file="Alimentation/Donnees/DTM/DTM_bigram.Rdata")
load(file="Alimentation/Donnees/questionnaire_alimentation.RData")

# Description de la variable qualitative 
# 1 : cuisine plus, mange mieux, plus sain
# 2 : grignotte, moins sain
# 3 : lien avec poids : pert, prise
# 4 : lien avec l'horaire, le mode de prise de repart
# 5 : mange moins, sans explication
# 6 : mange plus, sans explication


# liste des mots gardees:
#View(c(names(DTM_bigram),names(DTM)))

#############################################Création de la variable####################

#On fait un vecteur dans lequel on stocke les valeurs de la modalité de la variable
n = nrow(DTM)
vect <- rep(0,n)
qualite <- rep(NA,n)
mieux <- rep(0,n)
moins_bien <- rep(0,n)
decalage <- rep(0,n)
poids <- rep(0,n)
mange_moins<- rep(0,n) 
mange_plus<- rep(0,n) 
conflit=0

#########automatiser les modalités pour non######## 

for (i in 1:n) {
  
  uni <- DTM[i,]
  bi <- DTM_bigram[i,]

  if (bi["plus legume"] | bi['plus equilibre'] |uni["attent"] |  bi['plus equilibre'] | bi['repa equilibre'] #| bi["repa leger"]
      | bi['fait maison'] | uni['bio'] | uni['mieux'] |uni['vrai'] | uni["temp"]
      | bi['plus fruit'] | bi["plus leger"]
      | bi["pas alcool"] |bi["moins sucre"] |bi["moins gras"] |bi['moins viand'] 
      | uni['cuisine'] 
      ){
    mieux[i] <- 1
  }
  if (((uni["grignotage"] | bi['plus sucre']| bi["plus alcool"]| uni["gateaux"] |uni["dessert"] |bi["plus riche"]
      | uni["copieux"] ) & !(bi["fait attent"]) & !(bi['moins grignotage'])  & !(bi['grignotage moins']) )
      |  bi['moins legume'] # bi["moins frais"] 
      | (uni['trop']) | uni["aperitif"]
      | (uni["sucre"] & !(bi["moins sucre"]))
      | (uni["alcool"] & !(bi["pas alcool"]) & !(uni["arret"]))
   ) {
    moins_bien[i] <- 1
  }
  if (uni["tard"] | uni["heur"] | uni['horair'] | uni['fix'] | uni['midi'] | uni['soir']
      | uni["decaler"] |uni['restaurant'] | uni['cantin']  
      |uni['sandwich'] | bi['petit dejeun'] | uni["regulier"]){
    decalage[i] <-1
    
  }
  if (uni["prise"]| uni['perdr']| uni['poid'] |uni['regim'] | uni['exercic']| uni["kilos"]
      |uni["perdu"]| uni['calori'] | bi["reequilibrag aliment"] | uni['activit'] |bi["weight watcher"]
  ){
    poids[i] <- 1
  }
  
  if ((bi["mange moins"] | bi["moins mange"] | bi["peu moins"]) & sum(bi)<=2 
      & mieux[i]==0 & moins_bien[i]==0 ){
    mange_moins[i] <- 1
  }
  
  if (((bi["mange plus"] | (bi["peu plus"] & bi["mange plus"])) & sum(bi)<=2
       & mieux[i]==0 & moins_bien[i]==0 )){
    mange_plus[i] <- 1
  }
  
}

n = length(data$doc_id)
non_classe = which(mieux==0 & moins_bien==0 & poids ==0 & decalage ==0 & poids == 0 & decalage ==0 & mange_moins ==0 & mange_plus ==0 )
print(length(non_classe)/n)
# 24% non classes


data <- questionnaire[questionnaire$doc_id %in% as.numeric(rownames(DTM)),]
data$mieux <- mieux
print(table(mieux)[2]/n)
data$moins_bien <- moins_bien
print(table(poids)[2]/n)
data$poids <- poids
print(table(decalage)[2]/n)
data$decalage <- decalage
print(table(mange_moins)[2]/n)
data$mange_moins <- mange_moins
print(table(mange_plus)[2]/n)
data$mange_plus <- mange_plus


categories_alimentation <- data

# 1 : 28% (mange mieux)
# 2 : 12% (mange moins bien)
# 3 : 16 % (decalage, prise repas)
# 4 : 11 % (poids)
# 5 : 4 % (mange moins sans autre explication)
# 6 : 8 % (mange plus sans autre explication)
# 0 : 22% = pas classés



##############################################################################################
##############                  REGROUPEMENT CATEGORIES                   ####################
##############################################################################################

# Nouvelles categories :
# 1 - mange plus : 7%
# 2 - mange moins : 3.5%
# 3 - mange mieux : 25%
# 4 - mange moins bien : 18%
# 5 - autre : 44%

table(categories_alimentation$mieux,categories_alimentation$moins_bien) #93 conflits
table(categories_alimentation$moins_bien,categories_alimentation$mange_plus) # ok pour manger plus et manger moins

categories_alimentation$alimentation_nature_changement <- rep(5,nrow(categories_alimentation))
for (i in 1:nrow(categories_alimentation)){
  if (categories_alimentation$mange_plus[i]==1){
    categories_alimentation$alimentation_nature_changement[i]=1
  }
  else if (categories_alimentation$mange_moins[i]==1){
    categories_alimentation$alimentation_nature_changement[i]=2
  }
  else if (categories_alimentation$mieux[i]==1 & categories_alimentation$moins_bien[i]==0){
    categories_alimentation$alimentation_nature_changement[i]=3
  }
  else if (categories_alimentation$moins_bien[i]==1 & categories_alimentation$mieux[i]==0 ){
    categories_alimentation$alimentation_nature_changement[i]=4
  }
}
categories_alimentation$alimentation_nature_changement = as.factor(categories_alimentation$alimentation_nature_changement)

levels(categories_alimentation$alimentation_nature_changement) <- c("mange plus","mange moins","mange mieux","mange moins bien","autre chgt alimentaire")

save(categories_alimentation,file="Alimentation/Resultats/categories_alimentation.Rdata")

