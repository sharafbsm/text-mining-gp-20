#######################################################################################################
#####################                  VISUALISATION DES DONNEES             ##########################
#######################################################################################################
load(file="Alimentation/Donnees/donnees_alimentation_nettoyes.Rdata")
load(file="Alimentation/Donnees/frequences_mots/frequence_mot.Rdata") #mot par mot
load(file="Alimentation/Donnees/frequences_mots/frequence_bigram.Rdata") #bigrammes
load(file="Alimentation/Donnees/frequences_mots/frequence_trigram.Rdata") #trigrammes
load(file="Alimentation/Donnees/DTM/DTM.Rdata") #TDM (mot par mot)
load(file="Alimentation/Donnees/DTM/DTM_bigram.Rdata") #TDM (mot par mot)
load(file="Alimentation/Donnees/DTM/DTM_trigram.Rdata") #TDM (mot par mot)
library(wordcloud)

#######################################################################################################
###############################        ETAPE 1 : NUAGE DE MOTS        #################################
#######################################################################################################

set.seed(1234)

# Unigramme 
wordcloud(words = frequence_mot$word, freq = frequence_mot$freq, min.freq = 1,
          max.words=40, random.order=FALSE, rot.per=0.35, 
          colors=brewer.pal(8, "Dark2"))

# Bigrammes
wordcloud(words = names(frequence_bigram), freq = frequence_bigram, min.freq = 1,
          max.words=40, random.order=FALSE, rot.per=0.35, 
          colors=brewer.pal(8, "Dark2"))

# Trigrammes
wordcloud(words = names(frequence_trigram), freq = frequence_trigram, min.freq = 1,
          max.words=30, random.order=FALSE, rot.per=0.2, 
          colors=brewer.pal(5, "Dark2"))



#######################################################################################################
#######################     ETAPE 2 : Analyse factorielle           ###################################
#######################################################################################################

# Dataframe utilisé : DTM 
# Varibles= mots => variable quanti
# Individus = documents


# ESSAI 1 : ACP sur les 10 mots les plus frequents (on voit rien si on prends plus)
library(FactoMineR)

# Unigram
res.pca <- PCA(DTM[,frequence_mot[1:10,"word"]], scale.unit = FALSE, graph = TRUE)
barplot(res.pca$eig[,1]) #on prends 4 axes
plot.PCA(res.pca,choix="var",axes=c(1,2))
plot.PCA(res.pca,choix="var",axes=c(3,4))

# Bigram
res.pca <- PCA(DTM_bigram[,names(frequence_bigram[1:5])], scale.unit = FALSE, graph = TRUE)
barplot(res.pca$eig[,1]) # on prends 2 axes
plot.PCA(res.pca,choix="var",axes=c(1,2))


# Trigram
res.pca <- PCA(DTM_trigram[,names(frequence_trigram[1:5])], scale.unit = FALSE, graph = TRUE)
barplot(res.pca$eig[,1]) #on prends 2 axes
plot.PCA(res.pca,choix="var",axes=c(1,2))


#Conclusion : rien de ressort vraiment
# A essayer : ACM : modalité 0 ou 1 pour chaque mot 
#(au fond on s'en fout que la personne est mis "manger" 6 ou 2 fois dans la phrase)
# ACM dans le sens inverse: variable= mot et les ind = mots
# => permettrait de faire un plot des mots 
# Mais est-ce que ca a du sens?