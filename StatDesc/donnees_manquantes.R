
#importation
library(openxlsx)
data=read.xlsx("Donnees/data.xlsx")
data=data[-1,]

#visualisation des missings
install.packages("Amelia")
library(Amelia)
missmap(data, main = "Missing values vs observed")

#nombre de valeurs manquantes
missing=sapply(data[-1,],function(x) sum(is.na(x)))
missing=as.data.frame(missing)

#réponses
table(data$`Souffrez-vous.d'apnÃ©e.du.sommeil.?`, useNA="always")
table(data$`Avez-vous.Ã©tÃ©.infectÃ©(e).par.le.COVID-19.?`,useNA="always")
table(data$`Avez-vous.Ã©tÃ©.infectÃ©(e).par.le.COVID-19.?`, useNA="always")
table(data$`Depuis.le.dÃ©but.de.la.crise.sanitaire.du.Covid-19,.avez-vous.reÃ§u.des.conseils.sur.lâ€™utilisation/arrÃªt.de.votre.PPC.?`, useNA="always")
table(data$, useNA="always")
